<div class="pl-5 mb-3 form-check form-switch">
    <input class="form-check-input" type="checkbox" role="switch" id="{{ $name }}" name="{{ $name }}"
           value="{{ old($name) ?  : (isset($check) && $check == '1' ? '1' : '0') }}"
           {{ old($name) == 1 ? 'checked' : (isset($check) && $check == '1' ? 'checked' : '') }}
    >
    <label class="form-check-label" for="{{ $name }}">
        {{ isset($title) ? $title : 'Активно' }}
    </label>
    @error($name)
    <div class="col-12">
        <p class="text-danger">{{ $message }}</p>
    </div>
    @enderror
</div>
