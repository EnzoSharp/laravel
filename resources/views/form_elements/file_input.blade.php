<div class="form-group">
    <label for="{{ $name }}">{{ $title }}</label>
    <div class="input-group">
        <div class="custom-file">
            <input type="file" class="custom-file-input" id="{{ $name }}" name="{{ $name }}" value="{{ old($name) ? old($name) : '' }}" {{ isset($multi) ? 'multiple' : '' }}>
            <label class="custom-file-label" for="{{ $name }}">Выберите файл</label>
        </div>
        <div class="input-group-append">
            <span class="input-group-text">Загрузить</span>
        </div>
    </div>
    @error($name)
    <div class="col-12">
        <p class="text-danger">{{ $message }}</p>
    </div>
    @enderror
</div>
