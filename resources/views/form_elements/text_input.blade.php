@if($type == 'hidden')
    <input type="{{ $type }}" class="form-control" name="{{ $name }}" value="{{$value}}">
@else
    @if(isset($description))
        <div class="mb-3">
            <label for="{{ $name }}" class="form-label">{{ $title }}</label>
            <input type="{{ $type }}" class="form-control" name="{{ $name }}" id="{{ $name }}"
                   value="{{ old($name) ? old($name) : (isset($value) ? $value : '') }}"
                   @if(isset($description))
                   aria-describedby="{{ $name }}Help"
                @endif
            >
            @if(isset($description))
                <div id="{{ $name }}Help" class="form-text">{{ $description }}</div>
            @endif
            @error($name)
            <div class="col-12">
                <p class="text-danger">{{ $message }}</p>
            </div>
            @enderror
        </div>
    @else
        <div class="mb-3 input-group">
            <span class="input-group-text">{{ $title }}</span>
            @if(isset($idfeature))
                @if(isset($value))
                    <input type="{{ $type }}" class="form-control" name="{{ $name }}" id="{{ $name }}"
                           value="{{ old($namefeature.'.'.$idfeature) ? old($namefeature.'.'.$idfeature) : ($value ? $value : '') }}">
                @elseif(isset($object) && !$object->isEmpty())
                    <input type="{{ $type }}" class="form-control" name="{{ $name }}" id="{{ $name }}"
                           value="{{ old($namefeature.'.'.$idfeature) ? old($namefeature.'.'.$idfeature) : ($object[0]->value ? $object[0]->value : '') }}">
                @elseif(isset($object2) && !$object2->isEmpty())
                    <input type="{{ $type }}" class="form-control" name="{{ $name }}" id="{{ $name }}"
                           value="{{ old($namefeature.'.'.$idfeature) ? old($namefeature.'.'.$idfeature) : ($object2[0]->content ? $object2[0]->content : '') }}">
                @else
                    <input type="{{ $type }}" class="form-control" name="{{ $name }}" id="{{ $name }}"
                           value="{{ old($namefeature.'.'.$idfeature) ? old($namefeature.'.'.$idfeature) : (isset($value) ? ($value) : '') }}">
                @endif
                @error($name)
                <div class="col-12">
                    <p class="text-danger">{{ $message }}</p>
                </div>
                @enderror
            @else
                <input type="{{ $type }}" class="form-control" name="{{ $name }}" id="{{ $name }}"
                       value="{{ old($name) ? old($name) : (isset($value) ? $value : '') }}">
                @error($name)
                <div class="col-12">
                    <p class="text-danger">{{ $message }}</p>
                </div>
                @enderror
            @endif
        </div>
    @endif
@endif
