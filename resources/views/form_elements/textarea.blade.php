@if(isset($SCEditor))
    <div class="mb-3">
        <label class="form-label">{{ $title }}</label>
        @if(isset($idfeature))
            @if(isset($object) && !$object->isEmpty())
                <textarea class="summernoteES form-control"
                          name="{{ $name }}">{{ old($namefeature.'.'.$idfeature) ? old($namefeature.'.'.$idfeature) : ($object[0]->content ? $object[0]->content : '') }}</textarea>
            @else
                <textarea class="summernoteES form-control"
                          name="{{ $name }}">{{ old($namefeature.'.'.$idfeature) ? old($namefeature.'.'.$idfeature) : (isset($value) ? $value : '') }}</textarea>
            @endif
        @else
            <textarea class="summernoteES form-control"
                      name="{{ $name }}">{{ old($name) ? old($name) : (isset($value) ? $value : '') }}</textarea>
        @endif
        @else
            <div class="mb-3 input-group">
                <span class="input-group-text">{{ $title }}</span>
                @if(isset($idfeature))
                    @if(isset($object) && !$object->isEmpty())
                        <textarea class="form-control"
                                  name="{{ $name }}">{{ old($namefeature.'.'.$idfeature) ? old($namefeature.'.'.$idfeature) : ($object[0]->value ? $object[0]->value : '') }}</textarea>
                    @else
                    <textarea class="form-control"
                              name="{{ $name }}">{{ old($namefeature.'.'.$idfeature) ? old($namefeature.'.'.$idfeature) : (isset($value) ? $value : '') }}</textarea>
                    @endif
                @else
                    <textarea class="form-control"
                              name="{{ $name }}">{{ old($name) ? old($name) : (isset($value) ? $value : '') }}</textarea>
                @endif
                @endif
            </div>
