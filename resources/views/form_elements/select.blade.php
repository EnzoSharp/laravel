<div class="mb-3">
    <label class="form-label" for="{{ $name }}">{{ $title }}</label>
    <select id="{{ $name }}" class="form-select"
            name="{{ $name }}{{ (isset($is_multi) && $is_multi == '1' ? '[]' : '') }}" {{ (isset($is_multi) && $is_multi == '1' ? 'multiple' : '') }}>
        @foreach($list as $item)
            @if(isset($idfeature))
                @if(isset($value) && !$value->isEmpty())
                    @php
                        $selected = false;
                    @endphp
                    @foreach($value as $v)
                        @php
                            echo $item['value'] .'value';
                            echo $v->value . 'object';
                        @endphp
                        @if($item['value'] == $v->value)
                            @php
                                echo 'gg';
                                    $selected = true;
                            @endphp
                        @endif
                    @endforeach
                    @if(strlen($idfeature))
                        <option
                            {{ (collect(old($namefeature.'.'.$idfeature))->contains($item['value'])) ? 'selected="true"': ($selected ? 'selected="true"' : '') }} value="{{ $item['value'] }}">{{ $item['text'] }}</option>
                    @else
                        <option
                            {{ (collect(old($namefeature))->contains($item['value'])) ? 'selected="true"': ($selected ? 'selected="true"' : '') }} value="{{ $item['value'] }}">{{ $item['text'] }}</option>
                    @endif
                @elseif(isset($valueTitle) && !$valueTitle->isEmpty())
                    @php
                        $selected = false;
                    @endphp
                    @foreach($valueTitle as $v)
                        @if($item['value'] == $v->id)
                            @php
                                    $selected = true;
                            @endphp
                        @endif
                    @endforeach
                    @if(strlen($idfeature))
                        <option
                            {{ (collect(old($namefeature.'.'.$idfeature))->contains($item['value'])) ? 'selected="true"': ($selected ? 'selected="true"' : '') }} value="{{ $item['value'] }}">{{ $item['text'] }}</option>
                    @else
                        <option
                            {{ (collect(old($namefeature))->contains($item['value'])) ? 'selected="true"': ($selected ? 'selected="true"' : '') }} value="{{ $item['value'] }}">{{ $item['text'] }}</option>
                    @endif
                @else
                    @if(strlen($idfeature))
                        <option
                            {{ (collect(old($namefeature.'.'.$idfeature))->contains($item['value'])) ? 'selected':'' }} value="{{ $item['value'] }}">{{ $item['text'] }}</option>
                    @else
                        <option
                            {{ (collect(old($namefeature))->contains($item['value'])) ? 'selected':'' }} value="{{ $item['value'] }}">{{ $item['text'] }}</option>
                    @endif
                @endif
            @else
                <option
                    {{ old($name) == $item['value'] ? 'selected' : (($item['value'] == $selected && old($name) == null) ? 'selected' : '' ) }} value="{{ $item['value'] }}">{{ $item['text'] }}</option>
            @endif
        @endforeach
    </select>
    @error($name)
    <div class="col-12">
        <p class="text-danger">{{ $message }}</p>
    </div>
    @enderror
</div>
