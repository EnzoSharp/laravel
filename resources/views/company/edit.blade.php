@extends('layouts.main')

@section('title','Редактирование компании - ' . $company->name)

@section('header','Редактирование компании - ' . $company->name)
@section('breadcrumbs')
    <li class="breadcrumb-item"><a href="{{ route('home') }}">Главная</a></li>
    <li class="breadcrumb-item"><a href="{{ route('company.index') }}">Компании</a></li>
@endsection

@section('content')
    <div class="col-12">
        <form class="mt-5" action="{{ route('company.update', $company->id) }}" method="post" enctype="multipart/form-data">
            @csrf
            @method('patch')
            @include('form_elements.text_input', ['name'=>'name', 'title'=>'Название', 'type'=>'text', 'value'=>$company->name])
            @include('form_elements.text_input', ['name'=>'email', 'title'=>'E-mail', 'type'=>'email', 'value'=>$company->email])
            @include('form_elements.text_input', ['name'=>'address', 'title'=>'Адрес', 'type'=>'text', 'value'=>$company->address])
            @include('form_elements.file_input', ['name'=>'logo', 'title'=>'Логотип'])
            <button type="submit" class="btn btn-success">Редактировать</button>
        </form>
    </div>
    <div class="col-12 mt-5">
        <a class="btn btn-secondary" href="{{ route('company.index') }}">Назад</a>
    </div>
@endsection
