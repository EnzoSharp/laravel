@extends('layouts.main')

@section('title','Добавление компании')

@section('header','Добавление компании')
@section('breadcrumbs')
    <li class="breadcrumb-item"><a href="{{ route('home') }}">Главная</a></li>
    <li class="breadcrumb-item"><a href="{{ route('company.index') }}">Компании</a></li>
@endsection

@section('content')
    <div class="col-12">
        <form class="mt-5" action="{{ route('company.store') }}" method="post" enctype="multipart/form-data">
            @csrf
            @include('form_elements.text_input', ['name'=>'name', 'title'=>'Название', 'type'=>'text'])
            @include('form_elements.text_input', ['name'=>'email', 'title'=>'E-mail', 'type'=>'email'])
            @include('form_elements.text_input', ['name'=>'address', 'title'=>'Адрес', 'type'=>'text'])
            @include('form_elements.file_input', ['name'=>'logo', 'title'=>'Логотип'])
            <button type="submit" class="btn btn-success">Добавить</button>
        </form>
    </div>
    <div class="col-12 mt-5">
        <a class="btn btn-secondary" href="{{ route('company.index') }}">Назад</a>
    </div>
@endsection
