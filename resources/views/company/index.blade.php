@extends('layouts.main')

@section('title', 'Компании')

@section('header','Компании')
@section('breadcrumbs')
    <li class="breadcrumb-item"><a href="{{ route('home') }}">Главная</a></li>
    <li class="breadcrumb-item">Компании</li>
@endsection

@section('content')
    <div class="col-12 mt-3 mb-5">
        <a class="btn btn-primary" href="{{ route('company.create') }}">Добавить компанию</a>
    </div>
    <div class="container mt-5">
        <h2 class="mb-4">Компании</h2>
        <table class="table table-bordered yajra-datatable">
            <thead>
            <tr>
                <th>№</th>
                <th>Имя</th>
                <th>Email</th>
                <th>Дата добавления</th>
                <th>Действие</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
    <!-- Button trigger modal -->
    <button style="display: none" type="button" class="btn btn-primary" id="deleteCompanyButton" data-bs-toggle="modal" data-bs-target="#deleteCompany">
        Удалить компанию
    </button>

    <!-- Modal -->
    <div class="modal fade" id="deleteCompany" tabindex="-1" aria-labelledby="deleteCompanyLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="deleteCompanyLabel">Удалить компанию?</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">X</button>
                </div>
                <div class="modal-body">
                    Подтвердите, что Вы хотите удалить данную компанию!
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <form action="" method="post">
                        @csrf
                        @method('delete')
                        <button type="submit" class="btn btn-danger">Удалить</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('script')
    <script type="text/javascript">
        $(function () {

            var table = $('.yajra-datatable').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('company.index') }}",
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                    {data: 'name', name: 'name'},
                    {data: 'email', name: 'email'},
                    {data: 'created_at', name: 'created_at'},
                    {
                        data: 'action',
                        name: 'action',
                        orderable: true,
                        searchable: true
                    },
                ]
            });

        });
        $('.yajra-datatable').on('click', '.delete.btn.btn-danger', function (e) {
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var url = $(this).data('route');
            // confirm then
            $.ajax({
                url: url,
                type: 'DELETE',
                dataType: 'json',
                data: {method: '_DELETE', submit: true}
            }).always(function (data) {
                $('.yajra-datatable').DataTable().draw(false);
            });
        });
    </script>
@endsection
