@extends('layouts.main')

@section('title', 'Компания - '. $company->name)

@section('header','Компания - '. $company->name)
@section('breadcrumbs')
    <li class="breadcrumb-item"><a href="{{ route('home') }}">Главная</a></li>
    <li class="breadcrumb-item"><a href="{{ route('company.index') }}">Компании</a></li>
@endsection

@section('content')
    <div class="col-12 mt-3 mb-5">
        <a class="btn btn-secondary col-auto" href="{{ route('company.index') }}">Назад</a>
        <a class="btn btn-info col-auto" href="{{ route('company.edit', $company->id) }}">Редактировать</a>
        <div class="col-3"></div>
    </div>
    <div class="col-12 mb-5">
        <p>Название: <span>{{ $company->name }}</span></p>
        <p>Адрес: <span>{{ $company->address }}</span></p>
        <p>Сотрудники: </p>
            <ul>
                @foreach($company->users as $user)
                    <li>{{$user->name}}</li>
                @endforeach
            </ul>
        <p>Картинка: <span><img width="200" height="100%" src="{{ $company->logo != '' ? asset('/storage/'.$company->logo) : ''}}"></span></p>
        <p>Дата создания: <span>{{ $company->created_at }}</span></p>
        <p>Дата редактирования: <span>{{ $company->updated_at }}</span></p>
    </div>
    <div id="map" style="width: 600px; height: 400px"></div>
    <div class="row">
        <div class="col-10">

        </div>
        <div class="col-2">
            <form action="{{ route('company.delete', $company->id) }}" method="post">
                @csrf
                @method('delete')
                <button type="submit" class="btn btn-danger">Удалить</button>
            </form>
        </div>
    </div>
@endsection
@section('script')
    <script src="https://api-maps.yandex.ru/2.1/?apikey=6e7befaa-6a46-426a-b964-6a08d7743aca&lang=ru_RU" type="text/javascript">
    </script>
    <script type="text/javascript">
        let users = {!! $company->users !!};
        let usersList = '<ul>';
        for (let i = 0; i < users.length; i++) {
            usersList += '<li>'+users[i]['name']+'</li>';
        }
        usersList += '</ul>';
        console.log(usersList);
        // Функция ymaps.ready() будет вызвана, когда
        // загрузятся все компоненты API, а также когда будет готово DOM-дерево.
        ymaps.ready(init);
        function init(){
            // Создание карты.
            var myMap = new ymaps.Map("map", {
                // Координаты центра карты.
                // Порядок по умолчанию: «широта, долгота».
                // Чтобы не определять координаты центра карты вручную,
                // воспользуйтесь инструментом Определение координат.
                center: [{!! $latitude !!}, {!! $longitude !!}],
                // Уровень масштабирования. Допустимые значения:
                // от 0 (весь мир) до 19.
                zoom: 7
            });
            var myPlacemark = new ymaps.Placemark([{!! $latitude !!}, {!! $longitude !!}], {
                balloonContentBody: [
                    '<company>',
                    'Компания: <strong>{!! $company->name !!}</strong>',
                    '</company>',
                    '<br/>',
                    '<users>',
                    '<span>Сотрудники</span>',
                    '<br/>',
                    usersList,
                    '</users>'
                ].join('')
            }, {
                preset: 'islands#redDotIcon',
                iconColor: '#0095b6'
            });

            myMap.geoObjects.add(myPlacemark);
        }
    </script>
@endsection
