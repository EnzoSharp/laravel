@extends('layouts.main')

@section('title','Редактирование сотрудника - ' . $user->name)

@section('header','Редактирование сотрудника - ' . $user->name)
@section('breadcrumbs')
    <li class="breadcrumb-item"><a href="{{ route('home') }}">Главная</a></li>
    <li class="breadcrumb-item"><a href="{{ route('user.index') }}">Сотрудники</a></li>
@endsection

@section('content')
    <div class="col-12">
        <form class="mt-5" action="{{ route('user.update', $user->id) }}" method="post" enctype="multipart/form-data">
            @csrf
            @method('patch')
            @include('form_elements.text_input', ['name'=>'name', 'title'=>'ФИО', 'type'=>'text', 'value'=>$user->name])
            @include('form_elements.text_input', ['name'=>'email', 'title'=>'E-mail', 'type'=>'email', 'value'=>$user->email])
            @include('form_elements.text_input', ['name'=>'phone', 'title'=>'Телефон', 'type'=>'text', 'value'=>$user->phone])
            @include('form_elements.select', ['name'=>'company_id', 'title'=>'Род. категория', 'list'=>$company_array, 'selected'=>$user->company_id])
            <button type="submit" class="btn btn-success">Редактировать</button>
        </form>
    </div>
    <div class="col-12 mt-5">
        <a class="btn btn-secondary" href="{{ route('user.index') }}">Назад</a>
    </div>
@endsection
