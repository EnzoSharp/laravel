@extends('layouts.main')

@section('title', 'Компания - '. $user->name)

@section('header','Компания - '. $user->name)
@section('breadcrumbs')
    <li class="breadcrumb-item"><a href="{{ route('home') }}">Главная</a></li>
    <li class="breadcrumb-item"><a href="{{ route('user.index') }}">Сотрудники</a></li>
@endsection

@section('content')
    <div class="col-12 mt-3 mb-5">
        <a class="btn btn-secondary col-auto" href="{{ route('user.index') }}">Назад</a>
        <a class="btn btn-info col-auto" href="{{ route('user.edit', $user->id) }}">Редактировать</a>
        <div class="col-3"></div>
    </div>
    <div class="col-12 mb-5">
        <p>Название: <span>{{ $user->name }}</span></p>
        <p>Телефон: <span>{{ $user->phone }}</span></p>
        <p>E-mail: <span>{{ $user->email }}</span></p>
        <p>Компания: <span>{{ $user->company->name }}</span></p>
        <p>Дата создания: <span>{{ $user->created_at }}</span></p>
        <p>Дата редактирования: <span>{{ $user->updated_at }}</span></p>
    </div>
    <div class="row">
        <div class="col-10">

        </div>
        <div class="col-2">
            <form action="{{ route('user.delete', $user->id) }}" method="post">
                @csrf
                @method('delete')
                <button type="submit" class="btn btn-danger">Удалить</button>
            </form>
        </div>
    </div>
@endsection
