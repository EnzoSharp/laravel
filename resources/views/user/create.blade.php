@extends('layouts.main')

@section('title','Добавление компании')

@section('header','Добавление компании')
@section('breadcrumbs')
    <li class="breadcrumb-item"><a href="{{ route('home') }}">Главная</a></li>
    <li class="breadcrumb-item"><a href="{{ route('user.index') }}">Сотрудники</a></li>
@endsection

@section('content')
    <div class="col-12">
        <form class="mt-5" action="{{ route('user.store') }}" method="post" enctype="multipart/form-data">
            @csrf
            @include('form_elements.text_input', ['name'=>'name', 'title'=>'ФИО', 'type'=>'text'])
            @include('form_elements.text_input', ['name'=>'email', 'title'=>'E-mail', 'type'=>'email'])
            @include('form_elements.text_input', ['name'=>'phone', 'title'=>'Телефон', 'type'=>'text'])
            @include('form_elements.select', ['name'=>'company_id', 'title'=>'Род. категория', 'list'=>$company_array, 'selected'=>'0'])
            <button type="submit" class="btn btn-success">Добавить</button>
        </form>
    </div>
    <div class="col-12 mt-5">
        <a class="btn btn-secondary" href="{{ route('user.index') }}">Назад</a>
    </div>
@endsection
