<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes([
    'register' => false,
]);

Route::group(['namespace' => 'App\Http\Controllers', 'middleware'=>'admin'], function () {

    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

    Route::group(['namespace' => 'Company'], function () {
        Route::get('/companies', 'IndexController')->name('company.index');
        Route::get('/companies/create', 'CreateController')->name('company.create');
        Route::post('/companies', 'StoreController')->name('company.store');
        Route::get('/companies/{company}', 'ShowController')->name('company.show');
        Route::get('/companies/{company}/edit', 'EditController')->name('company.edit');
        Route::patch('/companies/{company}', 'UpdateController')->name('company.update');
        Route::delete('/companies/{company}', 'DestroyController')->name('company.delete');
    });

    Route::group(['namespace' => 'User'], function () {
        Route::get('/users', 'IndexController')->name('user.index');
        Route::get('/users/create', 'CreateController')->name('user.create');
        Route::post('/users', 'StoreController')->name('user.store');
        Route::get('/users/{user}', 'ShowController')->name('user.show');
        Route::get('/users/{user}/edit', 'EditController')->name('user.edit');
        Route::patch('/users/{user}', 'UpdateController')->name('user.update');
        Route::delete('/users/{user}', 'DestroyController')->name('user.delete');
    });

});
