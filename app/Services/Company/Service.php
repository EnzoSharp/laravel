<?php

namespace App\Services\Company;

use App\Http\Controllers\DownloadFilesController;
use App\Models\Company;

class Service
{

    public function store($data)
    {
        if (array_key_exists('logo', $data)) {
            $data['logo'] = DownloadFilesController::checkFileAndSave($data['logo'], 'company');
        }
        Company::create($data);
    }

    public function update($company, $data)
    {
        $company->update($data);
    }

}
