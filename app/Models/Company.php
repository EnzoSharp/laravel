<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kyslik\ColumnSortable\Sortable;

class Company extends Model
{
    use HasFactory;
    use Sortable;
    use SoftDeletes;

    protected $table = 'company';
    protected $guarded = false;

    public $sortable = ['id', 'name', 'email', 'created_at'];

    public function users()
    {
        return $this->hasMany(User::class, 'company_id', 'id');
    }
}
