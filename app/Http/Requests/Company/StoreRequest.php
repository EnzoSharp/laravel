<?php

namespace App\Http\Requests\Company;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'email' => 'required|string|email|unique:users',
            'address' => 'nullable|string',
            'logo' => [
                'nullable',
                'file',
                'mimes:jpeg,png,jpg,gif,svg',
                'max:2048',
                Rule::dimensions()->minWidth('100')->minHeight('100')
            ],
        ];
    }
}
