<?php

namespace App\Http\Controllers\Company;

use App\Models\Company;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use function view;

class IndexController extends BaseController
{
    public function __invoke(Request $request)
    {
        if ($request->ajax()) {
            $data = Company::latest()->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $actionBtn = '<a href="'.route('company.show',$row->id).'" class="btn btn-success btn-sm">Просмотр</a><a href="'.route('company.edit',$row->id).'" class="edit btn btn-warning btn-sm">Редактировать</a><a href="javascript:void(0)" data-route="'.route('company.delete',$row->id).'" class="delete btn btn-danger btn-sm">Удалить</a>';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
//        $companies = Company::sortable()->paginate(10);
//        return view('company.index', compact('companies'));
        return view('company.index');
    }
}
