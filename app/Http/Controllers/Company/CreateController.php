<?php

namespace App\Http\Controllers\Company;

use function view;

class CreateController extends BaseController
{
    public function __invoke()
    {
        return view('company.create');
    }
}
