<?php

namespace App\Http\Controllers\Company;

use App\Http\Controllers\GetAddressController;
use App\Models\Company;
use function view;

class ShowController extends BaseController
{
    public function __invoke(Company $company)
    {
        $coordinates = GetAddressController::getAddress($company->address);
        $latitude = $coordinates[1];
        $longitude = $coordinates[0];
        return view('company.show', compact('company', 'latitude', 'longitude'));
    }
}
