<?php

namespace App\Http\Controllers\company;

use App\Models\Company;
use function redirect;

class DestroyController extends BaseController
{
    public function __invoke(Company $company)
    {
        $company->delete();
        return redirect()->route('company.index');
    }
}
