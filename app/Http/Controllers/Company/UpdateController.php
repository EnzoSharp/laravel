<?php

namespace App\Http\Controllers\Company;

use App\Http\Requests\Company\UpdateRequest;
use App\Models\Company;
use function redirect;

class UpdateController extends BaseController
{
    public function __invoke(UpdateRequest $request, Company $company)
    {
        $data = $request->validated();
        $this->service->update($company, $data);
        return redirect()->route('company.show', $company->id);
    }
}
