<?php

namespace App\Http\Controllers\Company;

use App\Http\Requests\Company\StoreRequest;
use function redirect;

class StoreController extends BaseController
{
    public function __invoke(StoreRequest $request)
    {
        $data = $request->validated();
        $this->service->store($data);

        return redirect()->route('company.index');
    }
}
