<?php

namespace App\Http\Controllers\Company;

use App\Models\Company;
use function view;

class EditController extends BaseController
{
    public function __invoke(Company $company)
    {
        return view('company.edit', compact('company'));
    }
}
