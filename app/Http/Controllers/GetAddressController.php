<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GetAddressController extends Controller
{

    public static function getAddress($address)
    {

        $ch = curl_init('https://geocode-maps.yandex.ru/1.x/?apikey=6e7befaa-6a46-426a-b964-6a08d7743aca&format=json&geocode=' . urlencode($address));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HEADER, false);
        $res = curl_exec($ch);
        curl_close($ch);

        $res = json_decode($res, true);

        $coordinates = $res['response']['GeoObjectCollection']['featureMember'][0]['GeoObject']['Point']['pos'];
        return explode(' ', $coordinates);
    }

}
