<?php

namespace App\Http\Controllers\User;

use App\Models\User;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use function view;

class IndexController extends BaseController
{
    public function __invoke(Request $request)
    {
        if ($request->ajax()) {
            $data = User::where('role','user')->latest()->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $actionBtn = '<a href="'.route('user.show',$row->id).'" class="edit btn btn-success btn-sm">Просмотр</a><a href="'.route('user.edit',$row->id).'" class="edit btn btn-warning btn-sm">Редактировать</a><a href="javascript:void(0)" data-route="'.route('user.delete',$row->id).'" class="delete btn btn-danger btn-sm">Удалить</a>';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
//        $users = User::where('role','user')->paginate(10);
//        return view('user.index', compact('users'));
        return view('user.index');
    }
}
