<?php

namespace App\Http\Controllers\User;

use App\Models\Company;
use App\Models\User;
use function view;

class EditController extends BaseController
{
    public function __invoke(User $user)
    {
        $companies = Company::orderBy('name')->get();
        $company_array = array();
        if (!$companies->isEmpty()) {
            $company_array[] = array('value'=>'0','text'=>'Выберите компанию');
            foreach ($companies as $company) {
                $company_array[] = array(
                    'value' => $company->id,
                    'text' => $company->name,
                );
            }
        }
        return view('user.edit', compact('user', 'company_array'));
    }
}
