<?php

namespace App\Http\Controllers\User;

use App\Models\Company;
use function view;

class CreateController extends BaseController
{
    public function __invoke()
    {
        $companies = Company::orderBy('name')->get();
        $company_array = array();
        if (!$companies->isEmpty()) {
            $company_array[] = array('value'=>'0','text'=>'Выберите компанию');
            foreach ($companies as $company) {
                $company_array[] = array(
                    'value' => $company->id,
                    'text' => $company->name,
                );
            }
        }
        return view('user.create', compact('company_array'));
    }
}
