<?php

namespace App\Http\Controllers\user;

use App\Models\User;
use function redirect;

class DestroyController extends BaseController
{
    public function __invoke(User $user)
    {
        $user->delete();
        return redirect()->route('user.index');
    }
}
