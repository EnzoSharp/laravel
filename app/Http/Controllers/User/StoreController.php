<?php

namespace App\Http\Controllers\User;

use App\Http\Requests\User\StoreRequest;
use function redirect;

class StoreController extends BaseController
{
    public function __invoke(StoreRequest $request)
    {
        $data = $request->validated();
        $this->service->store($data);

        return redirect()->route('user.index');
    }
}
