<?php

namespace App\Http\Controllers;

use App\Models\File;
use Illuminate\Support\Facades\Storage;

class DownloadFilesController extends Controller
{
    public static function checkFileAndSave(object $fileArray, string $storage): string
    {
//        Делаю необходимые проверки и прочее, после сохраняю
        Storage::put("/public/" . $storage, $fileArray);
        return $storage.'/'.$fileArray->hashName();
    }
}
