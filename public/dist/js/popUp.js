function openPopUp(el, type, action) {
    if (document.getElementById(type)) {
        let popUpBlock = document.getElementById(type);
        console.log('button[data-bs-target="#' + type + '"]');
        document.getElementById(type + 'Button').click();
        popUpBlock.querySelector('form').action = action;
    }
}
